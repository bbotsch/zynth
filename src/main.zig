const std = @import("std");
const builtin = @import("builtin");
const clap = @import("clap");
const alsa = @import("alsa/alsa.zig");
const Zynth = @import("zynth/Zynth.zig");

var should_quit = false;
var format: alsa.Pcm.Format = .float_le;
var period_size: alsa.Pcm.Uframes = 0;
var channels: c_uint = 0;

pub const std_options = .{
    .log_level = .debug,
};

pub fn main() !void {
    setupQuitSignal();

    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .init;
    const allocator = if (builtin.mode == .Debug) gpa.allocator() else std.heap.c_allocator;
    defer if (builtin.mode == .Debug) std.debug.assert(gpa.deinit() == .ok);

    var stderr_buffered = std.io.bufferedWriter(std.io.getStdErr().writer());
    defer stderr_buffered.flush() catch {};
    const stderr = stderr_buffered.writer();

    const clap_params = comptime clap.parseParamsComptime(
        \\-h, --help                         Display this help and exit
        \\-d, --device <str>                 PCM device to use
        \\-m, --midi-port <str>              MIDI port to use
        \\-f, --format <Format>              Sample format, e.g. "s16_le", "s24_be", "float_le"
        \\-r, --rate <c_uint>                Sampling rate in hertz
        \\-b, --buffer-time <c_uint>         Buffer duration in microseconds
        \\-p, --period-time <c_uint>         Distance between interrupts in microseconds
        \\-i, --impulse-file <str>           File to use as reverb impulse response (must be raw float_le PCM)
        \\-n, --threads <usize>              Number of worker threads to spawn. 0 for single-threaded, defaults to number of CPUs.
    );
    const clap_result = clap.parse(
        clap.Help,
        &clap_params,
        comptime .{
            .str = clap.parsers.string,
            .c_uint = clap.parsers.int(u32, 10),
            .usize = clap.parsers.int(usize, 10),
            .Format = clap.parsers.enumeration(alsa.Pcm.Format),
        },
        .{ .allocator = allocator },
    ) catch {
        try stderr.print("Usage: {s} ", .{"zynth"});
        try clap.usage(stderr, clap.Help, &clap_params);
        try stderr.writeByte('\n');
        return;
    };
    defer clap_result.deinit();

    if (clap_result.args.help != 0) {
        try stderr.print("Usage: {s} ", .{"zynth"});
        try clap.usage(stderr, clap.Help, &clap_params);
        try stderr.print("\nOptions:\n", .{});
        try clap.help(stderr, clap.Help, &clap_params, .{});
        return;
    }

    const device_name = try allocator.dupeZ(u8, clap_result.args.device orelse "default");
    defer allocator.free(device_name);

    const pcm: alsa.Pcm = try .open(device_name, .playback, .block);
    defer pcm.close() catch |err| alsa.logError(err);
    std.log.info("Returned PCM: {}", .{pcm});
    std.log.info("PCM name: {s}", .{pcm.getName()});
    std.log.info("PCM type: {s}", .{@tagName(pcm.getType())});
    std.log.info("PCM state: {s}", .{@tagName(pcm.getState())});

    const hw_params: alsa.Pcm.HwParams = try .create(allocator);
    defer hw_params.destroy(allocator);
    try setHwParams(pcm, hw_params, clap_result.args);
    std.log.info("PCM state: {s}", .{@tagName(pcm.getState())});

    // const sw_params: alsa.Pcm.SwParams = try .create(allocator);
    // defer sw_params.destroy(allocator);
    // try setSwParams(pcm, sw_params);

    defer pcm.drain() catch |err| alsa.logError(err);

    const midi_port = try allocator.dupeZ(u8, clap_result.args.@"midi-port" orelse "default");
    defer allocator.free(midi_port);

    const midi: alsa.RawMidi = try .open(midi_port, 0);
    defer midi.close() catch |err| alsa.logError(err);
    try midi.nonblock(true);

    const midi_status: alsa.RawMidi.Status = try .create(allocator);
    defer midi_status.destroy(allocator);

    const midi_params: alsa.RawMidi.Params = try .create(allocator);
    defer midi_params.destroy(allocator);
    try setMidiParams(midi, midi_params);

    defer midi.drain() catch |err| alsa.logError(err);
    const midi_reader = midi.reader();

    var zynth: Zynth = undefined;
    try zynth.init(
        std.heap.page_allocator,
        .{ .sample_rate = clap_result.args.rate orelse 48000, .waveform = .smooth_saw },
        clap_result.args.threads,
    );
    defer zynth.deinit();

    if (clap_result.args.@"impulse-file") |filename| {
        const file = try std.fs.cwd().openFile(filename, .{});
        defer file.close();
        try zynth.loadImpulse(file.reader());
        std.log.info("Loaded impulse file: {s}", .{filename});
    }

    while (!should_quit) {
        _ = try zynth.processMidiMessages(midi_reader);

        const avail = pcm.availUpdate() catch |err| switch (err) {
            error.Pipe => {
                try pcm.recover(err, false);
                continue;
            },
            else => return err,
        };
        if (avail < period_size) {
            const state = pcm.getState();
            if (state == .prepared) {
                try pcm.start();
            }
            _ = try pcm.wait(-1);
            continue;
        }

        const mmap = try pcm.mmapBegin();
        const area = mmap.areas[0];
        switch (format) {
            inline else => |fmt| {
                const Sample = try fmt.SampleType();
                std.debug.assert(area.first == 0);
                std.debug.assert(area.step == (@sizeOf(Sample)) * 8);
                const buffer: [*]volatile Sample = @ptrCast(@alignCast(area.addr.?));
                zynth.generateWaveform(buffer[mmap.offset..][0..mmap.frames]);
            },
        }
        const committed = try pcm.mmapCommit(mmap);
        std.debug.assert(committed == mmap.frames);
    }
}

fn setMidiParams(midi: alsa.RawMidi, params: alsa.RawMidi.Params) !void {
    // Check to make sure midi params installation worked
    try params.current(midi);

    std.log.info("MIDI avail min: {}", .{params.getAvailMin()});
    std.log.info("MIDI buffer size: {}", .{params.getBufferSize()});
    std.log.info("MIDI clock type: {s}", .{@tagName(params.getClockType())});
    std.log.info("MIDI no active sensing: {}", .{params.getNoActiveSensing()});
    std.log.info("MIDI Read Mode: {}", .{params.getReadMode()});
}

fn setHwParams(pcm: alsa.Pcm, params: alsa.Pcm.HwParams, args: anytype) !void {
    try params.any(pcm);
    try params.setRateResample(pcm, false);
    try params.setAccess(pcm, .mmap_interleaved);
    try params.setFormat(pcm, args.format orelse .float_le);
    try params.setChannels(pcm, 1);
    _ = try params.setRateNear(pcm, args.rate orelse 48000, 0);
    _ = try params.setBufferTimeNear(pcm, args.@"buffer-time" orelse 5000, -1);
    _ = if (args.@"period-time") |period_time| try params.setPeriodTimeNear(pcm, period_time, -1);

    try pcm.hwParams(params);

    // Check to make sure hw params installation worked
    std.log.info("PCM rate resample: {}", .{try params.getRateResample(pcm)});
    std.log.info("PCM access: {s}", .{@tagName(try params.getAccess())});
    format = try params.getFormat();
    std.log.info("PCM format: {s}", .{@tagName(format)});
    std.log.info("PCM format width: {}", .{try format.physicalWidth()});
    std.log.info("PCM channels: {}", .{try params.getChannels()});
    std.log.info("PCM rate: {}", .{(try params.getRate()).val});
    std.log.info("PCM buffer time: {}", .{(try params.getBufferTime()).val});
    std.log.info("PCM buffer size: {}", .{try params.getBufferSize()});
    period_size = (try params.getPeriodSize()).val;
    std.log.info("PCM period size: {}", .{period_size});
}

fn setSwParams(pcm: alsa.Pcm, params: alsa.Pcm.SwParams) !void {
    try params.current(pcm);

    try pcm.swParams(params);

    std.log.info("PCM start threshold: {}", .{try params.getStartThreshold()});
    std.log.info("PCM stop threshold: {}", .{try params.getStopThreshold()});
    std.log.info("PCM period events: {}", .{try params.getPeriodEvent()});
}

fn setupQuitSignal() void {
    const sigint = 2;
    const action = std.mem.zeroInit(std.posix.Sigaction, .{
        .handler = .{
            .handler = quit,
        },
        .flags = 0x10000000, // SA_RESTART
    });
    std.posix.sigaction(sigint, &action, null);
}

fn quit(signum: c_int) callconv(.C) void {
    _ = signum;
    should_quit = true;
    std.log.debug("Quitting!", .{});
}
