const std = @import("std");

const Note = @This();

index: i8,
tuning: f32 = 440, // rename this, tuning is too overloaded with intonation and microtonal
octave_size: u8 = 12,

pub fn parse(comptime str: []const u8) std.fmt.ParseIntError!Note {
    var pos: usize = 0;

    var note: Note = .{
        .index = switch (str[pos]) {
            'C' => -9,
            'D' => -7,
            'E' => -5,
            'F' => -4,
            'G' => -2,
            'A' => 0,
            'B' => 2,
            else => return error.InvalidCharacter,
        },
    };
    pos += 1;

    switch (str[pos]) {
        '#' => {
            pos += 1;
            note.index += 1;
        },
        'b' => {
            pos += 1;
            note.index -= 1;
            // Check for double flat
            if (str[pos] == 'b') {
                pos += 1;
                note.index -= 1;
            }
        },
        'x' => {
            pos += 1;
            note.index += 2;
        },
        else => {},
    }

    const octave = try std.fmt.parseInt(i8, str[pos..], 10);
    var offset: i16 = octave - 4;
    offset *= 12;
    const new_index: i16 = note.index + offset;
    note.index = std.math.cast(i8, new_index) orelse return std.fmt.ParseIntError.Overflow;
    return note;
}

pub fn closestToFreqExt(f: f32, tuning: f32, octave_size: u8) Note {
    const exp: f32 = @log2(f / tuning);
    const size: f32 = @floatFromInt(octave_size);
    const index_approx = (size * exp);
    const index: i8 = @intFromFloat(@floor(index_approx + 0.5)); // +0.5 so that it rounds to the nearest integer
    return .{ .index = index };
}

pub inline fn closestToFreq(f: f32) Note {
    return closestToFreqExt(f, 440, 12);
}

pub fn freq(note: Note) f32 {
    const index: f32 = @floatFromInt(note.index);
    const octave_size: f32 = @floatFromInt(note.octave_size);
    return note.tuning * @exp2(index / octave_size);
}

pub const quarter_tone = @exp2(1.0 / 24.0) - 1;

test {
    const a4: Note = try .parse("A4");
    const c4: Note = try .parse("C4");
    const bs3: Note = try .parse("B#3");
    const cb4: Note = try .parse("Cb4");
    const b3: Note = try .parse("B3");
    const c7: Note = try .parse("C7");

    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("H4"));
    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("a4"));
    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("A##4"));
    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("Abbb4"));
    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("A#b4"));
    try std.testing.expectError(std.fmt.ParseIntError.InvalidCharacter, Note.parse("Ab#4"));
    try std.testing.expectError(std.fmt.ParseIntError.Overflow, Note.parse("F15"));
    try std.testing.expectError(std.fmt.ParseIntError.Overflow, Note.parse("C-6"));

    try std.testing.expectEqual(Note{ .index = 127 }, Note.parse("E15"));
    try std.testing.expectEqual(Note{ .index = -128 }, Note.parse("C#-6"));

    try std.testing.expectEqual(c4, bs3);
    try std.testing.expectEqual(b3, cb4);
    try std.testing.expectEqual(a4, Note.parse("Bbb4"));
    try std.testing.expectEqual(a4, Note.parse("Gx4"));

    try std.testing.expectEqual(0, a4.index);
    try std.testing.expectEqual(-9, c4.index);
    try std.testing.expectEqual(-9, bs3.index);
    try std.testing.expectEqual(-10, cb4.index);
    try std.testing.expectEqual(-10, b3.index);
    try std.testing.expectEqual(27, c7.index);

    try std.testing.expectApproxEqRel(440, a4.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(261.63, c4.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(261.63, bs3.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(246.94, cb4.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(246.94, b3.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(2093.00, c7.freq(), quarter_tone);

    const lowest: Note = .{ .index = std.math.minInt(i8) };
    const highest: Note = .{ .index = std.math.maxInt(i8) };
    try std.testing.expectApproxEqRel(0.270686, lowest.freq(), quarter_tone);
    try std.testing.expectApproxEqRel(675077, highest.freq(), quarter_tone);

    try std.testing.expectEqual(a4, Note.closestToFreq(440));
    try std.testing.expectEqual(c4, Note.closestToFreq(261.63));
    try std.testing.expectEqual(b3, Note.closestToFreq(246.94));
    try std.testing.expectEqual(c7, Note.closestToFreq(2093));
    try std.testing.expectEqual(c7, Note.closestToFreq(2033.5));
    try std.testing.expectEqual(c7, Note.closestToFreq(2154));
    try std.testing.expectEqual(Note.parse("Cb7"), Note.closestToFreq(2033.4));
    try std.testing.expectEqual(Note.parse("C#7"), Note.closestToFreq(2155));
}
