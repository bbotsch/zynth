const std = @import("std");

const vlen = std.simd.suggestVectorLength(f32) orelse 1;

pub const ImpulseParams = struct {
    sample_rate: f32,
    buffer_time: f32, // 0.5
    mu: f32, // 0.05
    sigma: f32, // 3.0 / 48000.0
    alpha: f32, // 0.85
};

/// Creates an impulse response buffer based on a repeating delay in the sound
/// Uses a normal distribution for each pulse with the first one centered at mu and standard deviation sigma
/// The amount the delay will decay each pulse is given by alpha
pub fn impulseResponse(allocator: std.mem.Allocator, params: ImpulseParams) ![]f32 {
    const buffer_len: usize = @intFromFloat(params.sample_rate * params.buffer_time);
    const impulse_response = try allocator.alloc(f32, buffer_len);

    const delta: f32 = 1 / params.sample_rate;

    var t: f32 = 0;
    var mu = params.mu;
    var alpha = params.alpha;
    var sigma = params.sigma;
    for (0..@intFromFloat(params.buffer_time / params.mu)) |_| {
        t = 0;
        mu += params.mu;
        alpha *= params.alpha;
        sigma += params.sigma;
        for (impulse_response) |*imp| {
            imp.* += alpha * delta * (@exp(-((t - mu) * (t - mu)) / (2 * sigma * sigma)) / @sqrt(std.math.tau * sigma * sigma));
            t += delta;
        }
    }

    return impulse_response;
}

pub fn readImpulseResponse(allocator: std.mem.Allocator, reader: anytype) ![]f32 {
    var impulse_response: std.ArrayListUnmanaged(f32) = .{};
    var sample: f32 = 0;
    while (reader.readAll(std.mem.asBytes(&sample))) |n| {
        if (n == 0) break; // EOF
        try impulse_response.append(allocator, sample);
    } else |err| return err;
    return try impulse_response.toOwnedSlice(allocator);
}

const Reverb = @This();

index: usize,
source: []f32,
impulse: []const f32,

pub const init: Reverb = .{
    .index = 0,
    .source = &.{},
    .impulse = &.{},
};

pub fn initWithParams(allocator: std.mem.Allocator, impulse_params: ImpulseParams) !Reverb {
    var reverb: Reverb = .init;
    reverb.impulse = try impulseResponse(allocator, impulse_params);
    std.mem.reverse(f32, reverb.impulse);
    reverb.source = try allocator.alloc(f32, reverb.impulse.len);
    @memset(reverb.source, 0);
    return reverb;
}

pub fn loadImpulse(self: *Reverb, allocator: std.mem.Allocator, reader: anytype) !void {
    self.deinit(allocator);
    self.* = .init;
    const impulse = try readImpulseResponse(allocator, reader);
    std.mem.reverse(f32, impulse);
    self.impulse = impulse;
    self.source = try allocator.alloc(f32, self.impulse.len);
    @memset(self.source, 0);
}

pub fn deinit(self: Reverb, allocator: std.mem.Allocator) void {
    allocator.free(self.source);
    allocator.free(self.impulse);
}

// Could we refrain from modifying self.source in this function and then just copy over the new source data after the region has all been calculated?
// What would the performance impact of this be and is it worth it?
pub fn apply(self: Reverb, value: f32, region_len: usize, offset: usize) f32 {
    var result = value;
    if (self.source.len < region_len) return result;

    var impulse_index = region_len - offset;
    var source_index = self.index + region_len;

    // Convolve data from self.index to self.source.len
    if (source_index < self.source.len) {
        if (self.source.len >= vlen) while (source_index <= (self.source.len - vlen)) : ({
            source_index += vlen;
            impulse_index += vlen;
        }) {
            const source: @Vector(vlen, f32) = self.source[source_index..][0..vlen].*;
            const impulse: @Vector(vlen, f32) = self.impulse[impulse_index..][0..vlen].*;
            result += @reduce(.Add, source * impulse);
        };
        // Convolve remaining data that doesn't fill up a whole vector
        var source: @Vector(vlen, f32) = @splat(0);
        for (0.., self.source[source_index..]) |i, val| source[i] = val;
        var impulse: @Vector(vlen, f32) = @splat(0);
        for (0..(self.source.len - source_index), self.impulse[impulse_index..][0..(self.source.len - source_index)]) |i, val| impulse[i] = val;
        result += @reduce(.Add, source * impulse);

        impulse_index += self.source.len - source_index;
        source_index = 0;
    } else {
        source_index %= self.source.len;
    }

    // Convolve data from beginning of self.source up to self.index
    if (self.index >= vlen) while (source_index <= (self.index - vlen)) : ({
        source_index += vlen;
        impulse_index += vlen;
    }) {
        const source: @Vector(vlen, f32) = self.source[source_index..][0..vlen].*;
        const impulse: @Vector(vlen, f32) = self.impulse[impulse_index..][0..vlen].*;
        result += @reduce(.Add, source * impulse);
    };
    // Convolve remaining data to that doesn't fill up a whole vector
    var source: @Vector(vlen, f32) = @splat(0);
    for (0.., self.source[source_index..self.index]) |i, val| source[i] = val;
    var impulse: @Vector(vlen, f32) = @splat(0);
    for (0..(self.index - source_index), self.impulse[impulse_index..][0..(self.index - source_index)]) |i, val| impulse[i] = val;
    result += @reduce(.Add, source * impulse);

    self.source[(self.index + offset) % self.source.len] = value;
    return result;
}

pub fn tick(self: *Reverb, frames: usize) void {
    if (self.source.len > 0) self.index = (self.index + frames) % self.source.len;
}
