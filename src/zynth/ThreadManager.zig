/// This file must be kept in sync with std's ThreadPool implementation since it relies on implementation details
/// (i.e. the fact that the only allocations done after initialization are for the jobs closures)
///
/// This type acts as a nice wrapper around closely related items such as a thread pool, it's corresponding wait group, and an arena allocator.
/// The arena allocator is meant to be reset after each round of jobs being spawned and waiting for them to finish.
/// You cannot achieve this by simply passing an ArenaAllocator to std.Thread.Pool because performing a reset
/// would wipe the memory allocated when initializing the pool such as the memory for the OS threads.
/// The arena is reset while retaining the capacity already allocated so that no memory allocations are performed after the first batch of jobs.
const std = @import("std");
const Allocator = std.mem.Allocator;

const log = std.log.scoped(.zynth);

const ThreadManager = @This();

pool: std.Thread.Pool,
wait_group: std.Thread.WaitGroup,
arena: std.heap.ArenaAllocator,

pub fn init(self: *ThreadManager, options: std.Thread.Pool.Options) !void {
    self.* = .{
        .pool = undefined,
        .wait_group = .{},
        .arena = .init(options.allocator),
    };
    try self.pool.init(options);

    // Switch thread pool allocator to our internal arena
    comptime var dummy_arena: std.heap.ArenaAllocator = undefined; // Only used to extract the private alloc() function from the ArenaAllocator implementation
    self.pool.allocator = .{
        .ptr = &self.arena,
        .vtable = &comptime .{
            .alloc = dummy_arena.allocator().vtable.alloc,
            .resize = &Allocator.noResize,
            .free = &Allocator.noFree,
        },
    };
}

pub fn deinit(self: *ThreadManager) void {
    // Switch thread pool allocator back to its original
    self.pool.allocator = self.arena.child_allocator;
    self.pool.deinit();
    self.arena.deinit();
}

pub fn spawn(self: *ThreadManager, comptime func: anytype, args: anytype) void {
    self.pool.spawnWg(&self.wait_group, func, args);
}

pub fn waitAndWork(self: *ThreadManager) void {
    self.pool.waitAndWork(&self.wait_group);
}

pub fn reset(self: *ThreadManager) void {
    self.wait_group.reset();
    const retained = self.arena.reset(.retain_capacity);
    if (!retained) log.warn("Failed to preheat thread pool allocator. Latency spikes may occur.", .{});
}
