const std = @import("std");
const zmath = @import("zmath");
const midi = @import("midi.zig");
const Note = @import("Note.zig");
const Reverb = @import("Reverb.zig");
const ThreadManager = @import("ThreadManager.zig");

const log = std.log.scoped(.zynth);
const vlen = std.simd.suggestVectorLength(f32) orelse 1;

fn multipleOfVectorLength(n: usize) usize {
    const chunks = std.math.divCeil(usize, n, vlen) catch unreachable;
    return chunks * vlen;
}

fn phasePerTick(rate: u32, freq: f32) f32 {
    const r: f32 = @floatFromInt(rate);
    return freq * std.math.tau / r;
}

// Rename this to Oscillator
// Ok, so technically we could have one big WaveInfo per instance of zynth and each key just has an index into the slices
// Could also make this into a MultiArrayList
pub const WaveInfo = struct {
    phase_per_tick: []f32,
    phase: []f32,

    fn tick(self: WaveInfo, n: usize) void {
        std.debug.assert(self.phase.len % vlen == 0);
        std.debug.assert(self.phase.len == self.phase_per_tick.len);
        const n_vec: @Vector(vlen, f32) = @splat(@floatFromInt(n));
        for (0..(self.phase.len / vlen)) |i| {
            const chunk = self.phase[(vlen * i)..][0..vlen];
            var phase: @Vector(vlen, f32) = chunk.*;
            const phase_per_tick: @Vector(vlen, f32) = self.phase_per_tick[(vlen * i)..][0..vlen].*;
            const phase_for_ticks = phase_per_tick * n_vec;
            phase = zmath.modAngle(phase + phase_for_ticks);
            chunk.* = phase;
        }
    }

    fn computeSample(self: WaveInfo, _harmonics: []const f32) f32 {
        var sum: f32 = 0;
        std.debug.assert(self.phase.len % vlen == 0);
        std.debug.assert(_harmonics.len >= self.phase.len);
        for (0..(self.phase.len / vlen)) |i| {
            const phases: @Vector(vlen, f32) = self.phase[(vlen * i)..][0..vlen].*;
            const harmonics: @Vector(vlen, f32) = _harmonics[(vlen * i)..][0..vlen].*;
            const values = zmath.sin(phases) * harmonics;
            sum += @reduce(.Add, values);
        }
        return sum;
    }
};

pub const Waveform = union(enum) {
    harmonics: []const f32,
    samples: []const f32,
    function: *const fn (phase: f32) f32,

    pub fn invExp(n: comptime_int, a: f32, odd_only: bool) [multipleOfVectorLength(n)]f32 {
        @setEvalBranchQuota(n * 100);
        var harmonics: [multipleOfVectorLength(n)]f32 = undefined;
        for (0..n) |i| {
            if ((odd_only) and (i % 2 != 0)) continue;
            const h: f32 = @floatFromInt(i + 1);
            harmonics[i] = a / std.math.pow(f32, a, h);
        }
        @memset(harmonics[n..], 0);
        return harmonics;
    }

    pub fn invPow(n: comptime_int, a: f32, odd_only: bool) [multipleOfVectorLength(n)]f32 {
        @setEvalBranchQuota(n * 100);
        var harmonics: [multipleOfVectorLength(n)]f32 = undefined;
        for (0..n) |i| {
            if ((odd_only) and (i % 2 != 0)) continue;
            const h: f32 = @floatFromInt(i + 1);
            harmonics[i] = 1 / std.math.pow(f32, h, a);
        }
        @memset(harmonics[n..], 0);
        return harmonics;
    }

    pub fn invPoly(n: comptime_int, a: f32, odd_only: bool) [multipleOfVectorLength(n)]f32 {
        @setEvalBranchQuota(n * 100);
        var harmonics: [multipleOfVectorLength(n)]f32 = undefined;
        for (0..n) |i| {
            if ((odd_only) and (i % 2 != 0)) continue;
            const h: f32 = @floatFromInt(i + 1);
            harmonics[i] = 1 / (2 * std.math.pow(f32, h, a) - h);
        }
        @memset(harmonics[n..], 0);
        return harmonics;
    }

    pub fn sawFn(phase: f32) f32 {
        return phase * 2 / std.math.tau;
    }

    pub fn squareFn(phase: f32) f32 {
        return if (phase > 0) 1 else -1;
    }

    pub const sine: Waveform = .{ .harmonics = &invPow(1, 1, false) };
    pub const saw: Waveform = .{ .harmonics = &invPow(1000, 1, false) };
    pub const square: Waveform = .{ .harmonics = &invPow(1000, 1, true) };
    pub const smooth_saw: Waveform = .{ .harmonics = &invPow(1000, std.math.sqrt2, false) };
    pub const smooth_square: Waveform = .{ .harmonics = &invPow(1000, std.math.sqrt2, true) };
    pub const saw_fn: Waveform = .{ .function = &sawFn };
    pub const square_fn: Waveform = .{ .function = &squareFn };

    pub fn samplesFromHarmonics(harmonics: []const f32, n: comptime_int) [n]f32 {
        if (@inComptime()) @setEvalBranchQuota(n * harmonics.len * 2);
        var samples: [n]f32 = undefined;
        for (0..n) |i| {
            const i_float: f32 = @floatFromInt(i);
            var value: f32 = 0;
            for (1.., harmonics) |h, amp| {
                const h_float: f32 = @floatFromInt(h);
                const phase = (i_float * h_float * std.math.tau) / n;
                value += @sin(phase) * amp;
            }
            samples[i] = value;
        }
        return samples;
    }

    pub fn samplesFromFunction(f: *const fn (f32) f32, n: comptime_int) [n]f32 {
        if (@inComptime()) @setEvalBranchQuota(n * 3);
        var samples: [n]f32 = undefined;
        for (0..n) |i| {
            const i_float: f32 = @floatFromInt(i);
            const phase = ((i_float / n) - 0.5) * std.math.tau;
            samples[i] = f(phase);
        }
        return samples;
    }

    pub fn sampled(self: Waveform) Waveform {
        return switch (self) {
            .harmonics => |harmonics| .{ .samples = &samplesFromHarmonics(harmonics, 3000) },
            .function => |f| .{ .samples = &samplesFromFunction(f, 3000) },
            .samples => self,
        };
    }
};

const MidiKey = struct {
    state: enum { off, on, hold },
    freq: f32,
    amp: f32,
    wave_info: []WaveInfo,

    fn on(self: *MidiKey, velocity: f32) bool {
        self.amp = @exp2((velocity - 1) * 8) * 0.01;
        const changed = if (self.state == .off) true else false;
        self.state = .on;
        return changed;
    }

    fn off(self: *MidiKey, damper: bool) bool {
        if (damper) {
            self.state = .hold;
            return false;
        }
        self.state = .off;
        return true;
    }
};

pub const Params = struct {
    sample_rate: u32,
    waveform: Waveform,
};

pub const State = struct {
    keyboard: [128]MidiKey,
    damper: bool,
    high_resolution_velocity: f32,
    active_keys: std.ArrayListUnmanaged(u8),

    fn init(state: *State, params: Params, allocator: std.mem.Allocator, thread_count: usize) !void {
        state.* = .{
            .keyboard = undefined,
            .damper = false,
            .high_resolution_velocity = 0,
            .active_keys = try .initCapacity(allocator, 128),
        };

        for (&state.keyboard, 0..) |*key, index| {
            const note: Note = .{ .index = @as(i8, @intCast(index)) - 69 };
            key.* = .{
                .state = .off,
                .freq = note.freq(),
                .amp = 0,
                .wave_info = try allocator.alloc(WaveInfo, thread_count),
            };

            switch (params.waveform) {
                .harmonics => |harmonics| {
                    const sample_rate_f32: f32 = @floatFromInt(params.sample_rate);
                    const max_harmonics: usize = @intFromFloat(@divTrunc(sample_rate_f32, 2 * key.freq));
                    const num_harmonics = @min(harmonics.len, max_harmonics);
                    const size = multipleOfVectorLength(num_harmonics);
                    for (key.wave_info) |*wave_info| {
                        wave_info.* = .{
                            .phase = try allocator.alloc(f32, size),
                            .phase_per_tick = try allocator.alloc(f32, size),
                        };
                        @memset(wave_info.phase, 0);
                        for (0..num_harmonics) |i| {
                            const n: f32 = @floatFromInt(i + 1);
                            wave_info.phase_per_tick[i] = phasePerTick(params.sample_rate, key.freq * n);
                        }
                        @memset(wave_info.phase_per_tick[num_harmonics..], 0);
                    }
                },
                .function, .samples => {
                    for (key.wave_info) |*wave_info| {
                        wave_info.* = .{
                            .phase = try allocator.alloc(f32, vlen),
                            .phase_per_tick = try allocator.alloc(f32, vlen),
                        };
                        wave_info.phase_per_tick[0] = phasePerTick(params.sample_rate, key.freq);
                    }
                },
            }
        }
    }

    pub fn keyOn(state: *State, key_index: u8, velocity: f32) void {
        const changed = state.keyboard[key_index].on(velocity + state.high_resolution_velocity);
        if (changed) state.active_keys.appendAssumeCapacity(key_index);
    }

    pub fn keyOff(state: *State, key_index: u8) void {
        const changed = state.keyboard[key_index].off(state.damper);
        if (changed) {
            for (0..state.active_keys.items.len) |i| {
                if (state.active_keys.items[i] == key_index) {
                    _ = state.active_keys.swapRemove(i);
                    break;
                }
            }
        }
    }

    pub fn setDamper(state: *State, damper: bool) void {
        state.damper = damper;
        if (!state.damper) {
            var i: usize = 0;
            while (i < state.active_keys.items.len) {
                const key_index = state.active_keys.items[i];
                if (state.keyboard[key_index].state == .hold) {
                    state.keyboard[key_index].state = .off;
                    _ = state.active_keys.swapRemove(i);
                    continue;
                }
                i += 1;
            }
        }
    }

    pub fn reset(state: *State) void {
        state.damper = false;
        state.high_resolution_velocity = 0;
        for (state.active_keys.items) |key_index| {
            state.keyboard[key_index].state = .off;
        }
        state.active_keys.clearRetainingCapacity();
    }
};

const Zynth = @This();

params: Params,
arena: std.heap.ArenaAllocator,
thread_manager: ThreadManager,
state: State,
reverb: Reverb,

pub fn init(self: *Zynth, _allocator: std.mem.Allocator, params: Params, threads: ?usize) !void {
    self.* = .{
        .params = params,
        .arena = .init(_allocator),
        .thread_manager = undefined,
        .state = undefined,
        .reverb = .init,
    };
    errdefer self.arena.deinit();
    const allocator = self.arena.allocator();

    try self.thread_manager.init(.{ .allocator = allocator, .n_jobs = threads });
    errdefer self.thread_manager.deinit();
    log.info("Spawned {} threads", .{self.thread_manager.pool.threads.len});

    try self.state.init(params, allocator, self.thread_manager.pool.getIdCount());
}

pub fn deinit(self: *Zynth) void {
    self.thread_manager.deinit();
    self.arena.deinit();
}

// Just a forwarding function until we flesh out modifier modules
pub fn loadImpulse(self: *Zynth, reader: anytype) !void {
    return self.reverb.loadImpulse(self.arena.allocator(), reader);
}

pub fn reverbParams(self: *Zynth, params: Reverb.ImpulseParams) !void {
    self.reverb = try .initWithParams(self.arena.allocator(), params);
}

fn eq(index: u8) f32 {
    const index_float: f32 = @floatFromInt(index);
    return 8 * @exp2(-index_float / 64.0);
}

fn fillWaveform(self: *Zynth, buffer: anytype, thread_number: usize, ticks_before: usize, ticks_after: usize) void {
    const state = self.state;

    for (state.active_keys.items) |index| {
        const key = state.keyboard[index];
        key.wave_info[thread_number].tick(ticks_before);
    }

    for (buffer, 0..) |*frame, offset| {
        var value: f32 = 0;
        switch (self.params.waveform) {
            .harmonics => |harmonics| {
                for (state.active_keys.items) |index| {
                    const key = state.keyboard[index];
                    const wave_info = key.wave_info[thread_number];
                    value += wave_info.computeSample(harmonics) * key.amp * eq(index);
                    wave_info.tick(1);
                }
            },
            .function => |f| {
                for (state.active_keys.items) |index| {
                    const key = state.keyboard[index];
                    const wave_info = key.wave_info[thread_number];
                    value += f(wave_info.phase[0]) * key.amp * eq(index);
                    wave_info.tick(1);
                }
            },
            .samples => |samples| {
                for (state.active_keys.items) |index| {
                    const key = state.keyboard[index];
                    const wave_info = key.wave_info[thread_number];
                    const slen: f32 = @floatFromInt(samples.len);
                    const proportion: f32 = (wave_info.phase[0] / std.math.tau) + 0.5;
                    const sample_index: usize = @intFromFloat(proportion * slen);
                    value += samples[sample_index] * key.amp * eq(index);
                    wave_info.tick(1);
                }
            },
        }
        value = self.reverb.apply(value, ticks_before + buffer.len + ticks_after, ticks_before + offset);

        const Sample = @typeInfo(@TypeOf(buffer)).pointer.child;
        switch (@typeInfo(Sample)) {
            .int => {
                const lower: f32 = std.math.minInt(Sample);
                const upper: f32 = std.math.maxInt(Sample);
                value = std.math.clamp(value * upper, lower, upper);
                frame.* = @intFromFloat(value);
            },
            .float => {
                value = std.math.clamp(value, -1, 1);
                frame.* = value;
            },
            else => unreachable,
        }
    }

    for (state.active_keys.items) |index| {
        const key = state.keyboard[index];
        key.wave_info[thread_number].tick(ticks_after);
    }
}

pub fn generateWaveform(self: *Zynth, buffer: anytype) void {
    var n = self.thread_manager.pool.getIdCount();
    var before: usize = 0;
    var after: usize = buffer.len;
    while (n > 0) {
        const frames = after / n;
        after -= frames;
        self.thread_manager.spawn(
            fillWaveform,
            .{ self, buffer[before..][0..frames], n - 1, before, after },
        );
        before += frames;
        n -= 1;
    }
    self.thread_manager.waitAndWork();
    self.thread_manager.reset();
    self.reverb.tick(buffer.len);
}

// Is it ever possible for a partial midi message to be received? Check alsa docs!
// How to handle the midi device becoming powered off/disconnected while running?
pub fn processMidiMessages(self: *Zynth, reader: anytype) !bool {
    var midi_buffer: [384]u8 = undefined;
    const nbytes = reader.read(&midi_buffer) catch |err| switch (err) {
        error.Again => return false,
        else => return err,
    };

    const state = &self.state;
    var index: usize = 0;
    while (index < nbytes) {
        const status = midi_buffer[index];
        const action: midi.Action = @enumFromInt(status >> 4);
        switch (action) {
            .note_on => {
                std.debug.assert(nbytes - index >= 3);
                const key_index: u8 = @intCast(midi_buffer[index + 1]);
                const value: f32 = @floatFromInt(midi_buffer[index + 2]);
                if (value == 0) state.keyOff(key_index) else state.keyOn(key_index, value / 128);
                index += 3;
            },
            .note_off => {
                std.debug.assert(nbytes - index >= 3);
                const key_index: u8 = @intCast(midi_buffer[index + 1]);
                state.keyOff(key_index);
                index += 3;
            },
            .key_pressure => {
                log.err("MIDI key pressure not supported", .{});
                index += 3;
            },
            .controller_change => {
                std.debug.assert(nbytes - index >= 3);
                const controller: midi.Controller = @enumFromInt(midi_buffer[index + 1]);
                const value = midi_buffer[index + 2];
                switch (controller) {
                    .damper_pedal => state.setDamper(value != 0),
                    .high_resolution_velocity_prefix => {
                        const velocity: f32 = @floatFromInt(value);
                        state.high_resolution_velocity = velocity / (128 * 128);
                    },
                    .all_sound_off => {
                        state.reset();
                    },
                    .reset_all_controllers => {
                        log.debug("reset all controllers", .{});
                    },
                    else => {
                        log.err("MIDI controller type not supported: {s}", .{@tagName(controller)});
                    },
                }
                index += 3;
            },
            .program_change => {
                log.err("MIDI program change not supported", .{});
                index += 2;
            },
            .channel_pressure => {
                log.err("MIDI channel pressure not supported", .{});
                index += 2;
            },
            .pitch_bend => {
                log.err("MIDI pitch bend not supported", .{});
                index += 3;
            },
            .special => {
                const special: midi.Special = @enumFromInt(status & 0x0f);
                switch (special) {
                    .system_exclusive => {
                        log.err("MIDI system exclusive not supported", .{});
                        return error.UnknownMessageLength;
                    },
                    .song_position => {
                        log.err("MIDI song position not supported", .{});
                        index += 3;
                    },
                    .song_select => {
                        log.err("MIDI song select not supported", .{});
                        index += 2;
                    },
                    .bus_select => {
                        log.err("MIDI bus select not supported", .{});
                        index += 2;
                    },
                    .tune_request => {
                        log.err("MIDI tune request not supported", .{});
                        index += 1;
                    },
                    .end_of_sysx => {
                        log.err("MIDI end of SysEx not supported", .{});
                        index += 1;
                    },
                    .timing_tick => {
                        log.err("MIDI timing tick not supported", .{});
                        index += 1;
                    },
                    .start_song => {
                        log.err("MIDI start_song not supported", .{});
                        index += 1;
                    },
                    .continue_song => {
                        log.err("MIDI continue song not supported", .{});
                        index += 1;
                    },
                    .stop_song => {
                        log.err("MIDI stop song not supported", .{});
                        index += 1;
                    },
                    .active_sensing => {
                        log.err("MIDI active sensing not supported", .{});
                        index += 1;
                    },
                    .system_reset => {
                        log.err("MIDI system reset not supported", .{});
                        index += 1;
                    },
                }
            },
        }
    }
    return true;
}
