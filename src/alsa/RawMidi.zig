const std = @import("std");
const c = @import("c.zig");
const errors = @import("error.zig");
const Error = errors.Error;
const checkError = errors.checkError;

const RawMidi = @This();

handle: *c.snd_rawmidi_t,

pub fn open(name: [:0]const u8, mode: c_int) Error!RawMidi {
    var rawmidi: RawMidi = undefined;
    const errnum = c.snd_rawmidi_open(@ptrCast(&rawmidi.handle), null, name, mode);
    try checkError(errnum);
    return rawmidi;
}

pub fn close(self: RawMidi) Error!void {
    const errnum = c.snd_rawmidi_close(self.handle);
    try checkError(errnum);
}

pub fn nonblock(self: RawMidi, enable: bool) Error!void {
    const errnum = c.snd_rawmidi_nonblock(self.handle, @intFromBool(enable));
    try checkError(errnum);
}

pub fn read(self: RawMidi, buffer: []u8) Error!usize {
    const count = c.snd_rawmidi_read(self.handle, buffer.ptr, buffer.len);
    try checkError(count);
    return @intCast(count);
}

pub const Reader = std.io.GenericReader(RawMidi, Error, read);

pub fn reader(self: RawMidi) Reader {
    return .{ .context = self };
}

pub const Status = struct {
    handle: *c.snd_rawmidi_status_t,

    pub inline fn sizeOf() usize {
        return c.snd_rawmidi_status_sizeof();
    }

    pub fn create(allocator: std.mem.Allocator) std.mem.Allocator.Error!Status {
        const bytes = try allocator.alloc(u8, sizeOf());
        @memset(bytes, 0);
        return .{ .handle = @ptrCast(bytes) };
    }

    pub fn destroy(self: Status, allocator: std.mem.Allocator) void {
        const ptr: [*]const u8 = @ptrCast(self.handle);
        allocator.free(ptr[0..sizeOf()]);
    }

    pub inline fn copy(dst: Status, src: Status) void {
        c.snd_rawmidi_status_copy(dst.handle, src.handle);
    }

    pub fn get(self: Status, midi: RawMidi) Error!void {
        const errnum = c.snd_rawmidi_status(midi.handle, self.handle);
        try checkError(errnum);
    }

    pub inline fn getAvail(self: Status) usize {
        return c.snd_rawmidi_status_get_avail(self.handle);
    }
};

pub const ReadMode = enum {
    standard,
    tstamp,
};

pub const Clock = enum {
    none,
    realtime,
    monotonic,
    monotonic_raw,
};

pub const Params = struct {
    handle: *c.snd_rawmidi_params_t,

    pub inline fn sizeOf() usize {
        return c.snd_rawmidi_params_sizeof();
    }

    pub fn create(allocator: std.mem.Allocator) std.mem.Allocator.Error!Params {
        const bytes = try allocator.alloc(u8, sizeOf());
        @memset(bytes, 0);
        return .{ .handle = @ptrCast(bytes) };
    }

    pub fn destroy(self: Params, allocator: std.mem.Allocator) void {
        const ptr: [*]const u8 = @ptrCast(self.handle);
        allocator.free(ptr[0..sizeOf()]);
    }

    pub inline fn copy(dst: Params, src: Params) void {
        c.snd_rawmidi_params_copy(dst.handle, src.handle);
    }

    pub fn current(self: Params, midi: RawMidi) Error!void {
        const errnum = c.snd_rawmidi_params_current(midi.handle, self.handle);
        try checkError(errnum);
    }

    pub fn getReadMode(self: Params) ReadMode {
        const mode = c.snd_rawmidi_params_get_read_mode(self.handle);
        return @enumFromInt(mode);
    }

    pub inline fn getAvailMin(self: Params) usize {
        return c.snd_rawmidi_params_get_avail_min(self.handle);
    }

    pub inline fn getBufferSize(self: Params) usize {
        return c.snd_rawmidi_params_get_buffer_size(self.handle);
    }

    pub fn getClockType(self: Params) Clock {
        const clock = c.snd_rawmidi_params_get_clock_type(self.handle);
        return @enumFromInt(clock);
    }

    pub fn getNoActiveSensing(self: Params) bool {
        const no_active_sensing = c.snd_rawmidi_params_get_no_active_sensing(self.handle);
        return no_active_sensing != 0;
    }
};

pub fn params(self: RawMidi, midi_params: Params) Error!void {
    const errnum = c.snd_rawmidi_params(self.handle, midi_params.handle);
    try checkError(errnum);
}

pub fn drain(self: RawMidi) Error!void {
    const errnum = c.snd_rawmidi_drain(self.handle);
    try checkError(errnum);
}

pub fn drop(self: RawMidi) Error!void {
    const errnum = c.snd_rawmidi_drop(self.handle);
    try checkError(errnum);
}
