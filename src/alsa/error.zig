const std = @import("std");
const builtin = @import("builtin");
const c = @import("c.zig");

pub const Error = error{ Pipe, StrPipe, BadFd, NoTty, NoDev, NoData, Busy, Again, Inval, NoEnt };

pub const ErrorCode = enum(c_int) {
    pipe = -c.EPIPE,
    str_pipe = -c.ESTRPIPE,
    bad_fd = -c.EBADFD,
    no_tty = -c.ENOTTY,
    no_dev = -c.ENODEV,
    no_data = -c.ENODATA,
    busy = -c.EBUSY,
    again = -c.EAGAIN,
    inval = -c.EINVAL,
    no_ent = -c.ENOENT,
    _,

    pub inline fn toError(self: ErrorCode) Error {
        return switch (self) {
            .pipe => error.Pipe,
            .str_pipe => error.StrPipe,
            .bad_fd => error.BadFd,
            .no_tty => error.NoTty,
            .no_dev => error.NoDev,
            .no_data => error.NoData,
            .busy => error.Busy,
            .again => error.Again,
            .inval => error.Inval,
            .no_ent => error.NoEnt,
            else => {
                if (builtin.mode == .Debug) {
                    std.debug.panic(
                        "Unknown error ({}): {s}",
                        .{ @intFromEnum(self), c.snd_strerror(@intFromEnum(self)) },
                    );
                } else unreachable;
            },
        };
    }

    pub inline fn fromError(err: Error) ErrorCode {
        return switch (err) {
            error.Pipe => .pipe,
            error.StrPipe => .str_pipe,
            error.BadFd => .bad_fd,
            error.NoTty => .no_tty,
            error.NoDev => .no_dev,
            error.NoData => .no_data,
            error.Busy => .busy,
            error.Again => .again,
            error.Inval => .inval,
            error.NoEnt => .no_ent,
        };
    }
};

pub inline fn checkError(errnum: anytype) Error!void {
    if (errnum < 0) {
        const error_code: ErrorCode = @enumFromInt(errnum);
        return error_code.toError();
    }
}

pub inline fn logError(err: Error) void {
    const err_code: ErrorCode = .fromError(err);
    std.log.err("Alsa Error: {s}", .{c.snd_strerror(@intFromEnum(err_code))}); // Scope this error
}
