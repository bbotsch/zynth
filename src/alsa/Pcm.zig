const std = @import("std");
const builtin = @import("builtin");
const c = @import("c.zig");

const errors = @import("error.zig");
const Error = errors.Error;
const ErrorCode = errors.ErrorCode;
const checkError = errors.checkError;

const Pcm = @This();

handle: *c.snd_pcm_t,

pub const Mode = enum {
    block,
    nonblock,
    async_,
};

pub const Stream = enum {
    playback,
    capture,
};

pub const Type = enum {
    hw,
    hooks,
    multi,
    file,
    null,
    shm,
    inet,
    copy,
    linear,
    alaw,
    mulaw,
    adpcm,
    rate,
    route,
    plug,
    share,
    meter,
    mix,
    droute,
    lbserver,
    linear_float,
    ladspa,
    dmix,
    jack,
    dsnoop,
    dshare,
    iec958,
    softvol,
    ioplug,
    extplug,
    mmap_emul,
};

pub const State = enum {
    open,
    setup,
    prepared,
    running,
    xrun,
    draining,
    paused,
    suspended,
    disconnected,
};

pub const Format = enum(c_int) {
    unknown = -1,
    s8,
    u8,
    s16_le,
    s16_be,
    u16_le,
    u16_be,
    s24_le,
    s24_be,
    u24_le,
    u24_be,
    s32_le,
    s32_be,
    u32_le,
    u32_be,
    float_le,
    float_be,
    float64_le,
    float64_be,
    iec958_subframe_le,
    iec958_subframe_be,
    mu_law,
    a_law,
    ima_adpcm,
    mpeg,
    gsm,
    s20_le,
    s20_be,
    u20_le,
    u20_be,
    special = 31,
    s24_3le,
    s24_3be,
    u24_3le,
    u24_3be,
    s20_3le,
    s20_3be,
    u20_3le,
    u20_3be,
    s18_3le,
    s18_3be,
    u18_3le,
    u18_3be,
    g723_24,
    g723_24_1b,
    g723_40,
    g723_40_1b,
    dsd_u8,
    dsd_u16_le,
    dsd_u32_le,
    dsd_u16_be,
    dsd_u32_be,

    pub fn physicalWidth(self: Format) Error!c_int {
        const width = c.snd_pcm_format_physical_width(@intFromEnum(self));
        try checkError(width);
        return width;
    }

    pub const FormatError = error{FormatNotSupported};
    pub fn SampleType(self: Format) FormatError!type {
        switch (builtin.cpu.arch.endian()) {
            .big => {
                return switch (self) {
                    .s8 => i8,
                    .u8 => u8,
                    .s16_be => i16,
                    .u16_be => u16,
                    .s24_be => i24,
                    .u24_be => u24,
                    .s32_be => i32,
                    .u32_be => u32,
                    .float_be => f32,
                    .float64_be => f64,
                    .s20_be => i20,
                    .u20_be => u20,
                    else => error.FormatNotSupported,
                };
            },
            .little => {
                return switch (self) {
                    .s8 => i8,
                    .u8 => u8,
                    .s16_le => i16,
                    .u16_le => u16,
                    .s24_le => i24,
                    .u24_le => u24,
                    .s32_le => i32,
                    .u32_le => u32,
                    .float_le => f32,
                    .float64_le => f64,
                    .s20_le => i20,
                    .u20_le => u20,
                    else => error.FormatNotSupported,
                };
            },
        }
    }
};

pub const Access = enum {
    mmap_interleaved,
    mmap_noninterleaved,
    mmap_complex,
    rw_interleaved,
    rw_noninterleaved,
};

pub const Uframes = c.snd_pcm_uframes_t;
pub const Sframes = c.snd_pcm_sframes_t;

pub fn open(name: [:0]const u8, stream: Stream, mode: Mode) Error!Pcm {
    var pcm: Pcm = undefined;
    const errnum = c.snd_pcm_open(@ptrCast(&pcm.handle), name, @intFromEnum(stream), @intFromEnum(mode));
    try checkError(errnum);
    return pcm;
}

pub fn close(self: Pcm) Error!void {
    const errnum = c.snd_pcm_close(self.handle);
    try checkError(errnum);
}

pub const Nonblock = enum { disable, enable, abort };

pub fn nonblock(self: Pcm, enable: Nonblock) Error!void {
    const errnum = c.snd_pcm_nonblock(self.handle, @intFromEnum(enable));
    try checkError(errnum);
}

pub inline fn getName(self: Pcm) [:0]const u8 {
    const name = c.snd_pcm_name(self.handle);
    const len = std.mem.indexOfSentinel(u8, 0, name);
    return name[0..len :0];
}

pub inline fn getType(self: Pcm) Type {
    return @enumFromInt(c.snd_pcm_type(self.handle));
}

pub inline fn getState(self: Pcm) State {
    return @enumFromInt(c.snd_pcm_state(self.handle));
}

pub fn prepare(self: Pcm) Error!void {
    const errnum = c.snd_pcm_prepare(self.handle);
    try checkError(errnum);
}

pub fn start(self: Pcm) Error!void {
    const errnum = c.snd_pcm_start(self.handle);
    try checkError(errnum);
}

pub fn pause(self: Pcm, enable: bool) Error!void {
    const errnum = c.snd_pcm.pause(self.handle, @intFromBool(enable));
    try checkError(errnum);
}

pub fn wait(self: Pcm, timeout: c_int) Error!bool {
    const errnum = c.snd_pcm_wait(self.handle, timeout);
    try checkError(errnum);
    return errnum != 0;
}

pub fn drain(self: Pcm) Error!void {
    const errnum = c.snd_pcm_drain(self.handle);
    try checkError(errnum);
}

pub fn drop(self: Pcm) Error!void {
    const errnum = c.snd_pcm_drop(self.handle);
    try checkError(errnum);
}

pub fn recover(self: Pcm, err: Error, silent: bool) Error!void {
    const err_code = ErrorCode.fromError(err);
    const errnum = c.snd_pcm_recover(self.handle, @intFromEnum(err_code), @intFromBool(silent));
    try checkError(errnum);
}

pub const HwParams = struct {
    handle: *c.snd_pcm_hw_params_t,

    inline fn sizeOf() usize {
        return c.snd_pcm_hw_params_sizeof();
    }

    pub fn create(allocator: std.mem.Allocator) std.mem.Allocator.Error!HwParams {
        const bytes = try allocator.alloc(u8, sizeOf());
        @memset(bytes, 0);
        return .{ .handle = @ptrCast(bytes) };
    }

    pub fn destroy(self: HwParams, allocator: std.mem.Allocator) void {
        const ptr: [*]const u8 = @ptrCast(self.handle);
        allocator.free(ptr[0..sizeOf()]);
    }

    pub fn copy(dst: HwParams, src: HwParams) Error!HwParams {
        c.snd_pcm_hw_params_copy(dst.handle, src.handle);
    }

    pub fn any(self: HwParams, pcm: Pcm) Error!void {
        const errnum = c.snd_pcm_hw_params_any(pcm.handle, self.handle);
        try checkError(errnum);
    }

    pub fn current(self: HwParams, pcm: Pcm) Error!void {
        const errnum = c.snd_pcm_hw_params_current(pcm.handle, self.handle);
        try checkError(errnum);
    }

    pub fn getFormat(self: HwParams) Error!Format {
        var format: c.snd_pcm_format_t = undefined;
        const errnum = c.snd_pcm_hw_params_get_format(self.handle, &format);
        try checkError(errnum);
        return @enumFromInt(format);
    }

    pub fn setFormat(self: HwParams, pcm: Pcm, format: Format) Error!void {
        const errnum = c.snd_pcm_hw_params_set_format(pcm.handle, self.handle, @intFromEnum(format));
        try checkError(errnum);
    }

    pub fn getRateResample(self: HwParams, pcm: Pcm) Error!bool {
        var resample: c_uint = undefined;
        const errnum = c.snd_pcm_hw_params_get_rate_resample(pcm.handle, self.handle, @ptrCast(&resample));
        try checkError(errnum);
        return resample != 0;
    }

    pub fn setRateResample(self: HwParams, pcm: Pcm, resample: bool) Error!void {
        const errnum = c.snd_pcm_hw_params_set_rate_resample(pcm.handle, self.handle, @intFromBool(resample));
        try checkError(errnum);
    }

    pub fn getAccess(self: HwParams) Error!Access {
        var access: c.snd_pcm_access_t = undefined;
        const errnum = c.snd_pcm_hw_params_get_access(self.handle, &access);
        try checkError(errnum);
        return @enumFromInt(access);
    }

    pub fn setAccess(self: HwParams, pcm: Pcm, access: Access) Error!void {
        const errnum = c.snd_pcm_hw_params_set_access(pcm.handle, self.handle, @intFromEnum(access));
        try checkError(errnum);
    }

    pub fn getChannels(self: HwParams) Error!c_uint {
        var channels: c_uint = undefined;
        const errnum = c.snd_pcm_hw_params_get_channels(self.handle, &channels);
        try checkError(errnum);
        return channels;
    }

    pub fn setChannels(self: HwParams, pcm: Pcm, channels: c_uint) Error!void {
        const errnum = c.snd_pcm_hw_params_set_channels(pcm.handle, self.handle, channels);
        try checkError(errnum);
    }

    pub fn ApproxVal(val_type: type) type {
        return struct {
            val: val_type,
            dir: c_int,
        };
    }

    pub fn getRate(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_rate(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn getRateMin(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_rate_min(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn getRateMax(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_rate_max(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn setRateNear(self: HwParams, pcm: Pcm, rrate: c_uint, dir: c_int) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = .{ .val = rrate, .dir = dir };
        const errnum = c.snd_pcm_hw_params_set_rate_near(pcm.handle, self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn getBufferTime(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_buffer_time(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn getBufferTimeMin(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_buffer_time_min(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn getBufferTimeMax(self: HwParams) Error!ApproxVal(c_uint) {
        var rate: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_buffer_time_max(self.handle, &rate.val, &rate.dir);
        try checkError(errnum);
        return rate;
    }

    pub fn setBufferTimeNear(self: HwParams, pcm: Pcm, us: c_uint, dir: c_int) Error!ApproxVal(c_uint) {
        var buffer_time: ApproxVal(c_uint) = .{ .val = us, .dir = dir };
        const errnum = c.snd_pcm_hw_params_set_buffer_time_near(pcm.handle, self.handle, &buffer_time.val, &buffer_time.dir);
        try checkError(errnum);
        return buffer_time;
    }

    pub fn getBufferSize(self: HwParams) Error!Uframes {
        var uframes: Uframes = undefined;
        const errnum = c.snd_pcm_hw_params_get_buffer_size(self.handle, &uframes);
        try checkError(errnum);
        return uframes;
    }

    pub fn setBufferSize(self: HwParams, pcm: Pcm, uframes: Uframes) Error!void {
        const errnum = c.snd_pcm_hw_params_get_buffer_size(pcm.handle, self.handle, uframes);
        try checkError(errnum);
    }

    pub fn getPeriodTimeNear(self: HwParams) Error!ApproxVal(c_uint) {
        var period_time: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_period_time_near(self.handle, &period_time.val, &period_time.dir);
        try checkError(errnum);
        return period_time;
    }

    pub fn getPeriodTimeNearMin(self: HwParams) Error!ApproxVal(c_uint) {
        var period_time: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_period_time_min(self.handle, &period_time.val, &period_time.dir);
        try checkError(errnum);
        return period_time;
    }

    pub fn getPeriodTimeMax(self: HwParams) Error!ApproxVal(c_uint) {
        var period_time: ApproxVal(c_uint) = undefined;
        const errnum = c.snd_pcm_hw_params_get_period_time_max(self.handle, &period_time.val, &period_time.dir);
        try checkError(errnum);
        return period_time;
    }

    pub fn setPeriodTimeNear(self: HwParams, pcm: Pcm, us: c_uint, dir: c_int) Error!ApproxVal(c_uint) {
        var period_time: ApproxVal(c_uint) = .{ .val = us, .dir = dir };
        const errnum = c.snd_pcm_hw_params_set_period_time_near(pcm.handle, self.handle, &period_time.val, &period_time.dir);
        try checkError(errnum);
        return period_time;
    }

    pub fn getPeriodSize(self: HwParams) Error!ApproxVal(Uframes) {
        var size: ApproxVal(Uframes) = undefined;
        const errnum = c.snd_pcm_hw_params_get_period_size(self.handle, &size.val, &size.dir);
        try checkError(errnum);
        return size;
    }

    pub fn setPeriodSizeNear(self: HwParams, pcm: Pcm, frames: Uframes, dir: c_int) Error!ApproxVal(Uframes) {
        var size: ApproxVal(Uframes) = .{ .val = frames, .dir = dir };
        const errnum = c.snd_pcm_hw_params_set_period_size_near(pcm.handle, self.handle, &size.val, &size.dir);
        try checkError(errnum);
        return size;
    }
};

pub fn hwParams(self: Pcm, params: HwParams) Error!void {
    const errnum = c.snd_pcm_hw_params(self.handle, params.handle);
    try checkError(errnum);
}

pub fn hwFree(self: Pcm) Error!void {
    const errnum = c.snd_pcm_hw_free(self.handle);
    try checkError(errnum);
}

pub const SwParams = struct {
    handle: *c.snd_pcm_sw_params_t,

    inline fn sizeOf() usize {
        return c.snd_pcm_sw_params_sizeof();
    }

    pub fn create(allocator: std.mem.Allocator) std.mem.Allocator.Error!SwParams {
        const bytes = try allocator.alloc(u8, sizeOf());
        @memset(bytes, 0);
        return .{ .handle = @ptrCast(bytes) };
    }

    pub fn destroy(self: SwParams, allocator: std.mem.Allocator) void {
        const ptr: [*]const u8 = @ptrCast(self.handle);
        allocator.free(ptr[0..sizeOf()]);
    }

    pub fn copy(dst: SwParams, src: SwParams) Error!SwParams {
        c.snd_pcm_sw_params_copy(dst.handle, src.handle);
    }

    pub fn any(self: SwParams, pcm: Pcm) Error!void {
        const errnum = c.snd_pcm_sw_params_any(pcm.handle, self.handle);
        try checkError(errnum);
    }

    pub fn current(self: SwParams, pcm: Pcm) Error!void {
        const errnum = c.snd_pcm_sw_params_current(pcm.handle, self.handle);
        try checkError(errnum);
    }

    pub fn getStartThreshold(self: SwParams) Error!Uframes {
        var uframes: Uframes = undefined;
        const errnum = c.snd_pcm_sw_params_get_start_threshold(self.handle, &uframes);
        try checkError(errnum);
        return uframes;
    }

    pub fn getStopThreshold(self: SwParams) Error!Uframes {
        var uframes: Uframes = undefined;
        const errnum = c.snd_pcm_sw_params_get_stop_threshold(self.handle, &uframes);
        try checkError(errnum);
        return uframes;
    }

    pub fn getPeriodEvent(self: SwParams) Error!bool {
        var state: c_int = undefined;
        const errnum = c.snd_pcm_sw_params_get_period_event(self.handle, &state);
        try checkError(errnum);
        return state != 0;
    }
};

pub fn swParams(self: Pcm, params: SwParams) Error!void {
    const errnum = c.snd_pcm_sw_params(self.handle, params.handle);
    try checkError(errnum);
}

pub fn mmapWriteInterleaved(self: Pcm, buffer: anytype) Error!Uframes {
    const frames = c.snd_pcm_mmap_writei(self.handle, buffer.ptr, buffer.len);
    try checkError(frames);
    return @intCast(frames);
}

pub fn availUpdate(self: Pcm) Error!Uframes {
    const frames = c.snd_pcm_avail_update(self.handle);
    try checkError(frames);
    return @intCast(frames);
}

pub const ChannelArea = c.snd_pcm_channel_area_t;

pub const MmapRegion = struct {
    areas: [*]const ChannelArea,
    offset: Uframes,
    frames: Uframes,
};

pub fn mmapBegin(self: Pcm) Error!MmapRegion {
    var mmap: MmapRegion = undefined;
    const errnum = c.snd_pcm_mmap_begin(self.handle, @ptrCast(&mmap.areas), &mmap.offset, &mmap.frames);
    try checkError(errnum);
    return mmap;
}

pub fn mmapCommit(self: Pcm, mmap: MmapRegion) Error!Uframes {
    const count = c.snd_pcm_mmap_commit(self.handle, mmap.offset, mmap.frames);
    try checkError(count);
    return @intCast(count);
}
