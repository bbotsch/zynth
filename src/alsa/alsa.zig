const std = @import("std");
const errors = @import("error.zig");

pub const Error = errors.Error;
pub const logError = errors.logError;

pub const Pcm = @import("Pcm.zig");
pub const RawMidi = @import("RawMidi.zig");
